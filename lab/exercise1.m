clear
close all
% load the data
load Data
% add noise to the encoder signal and estimate position
noise = randn(size(Enc))/5;
EncPos = position_from_encoder(Enc+noise);
% plot the data
figure(1); hold on;
plot(RefPos(:,1),RefPos(:,2),'k*'); % reference data
plot(EncPos(:,1),EncPos(:,2),'r*'); % from encoder signal
plot(CamPos(:,1),CamPos(:,2),'m*'); % from camera
plot(DistSensPos(:,1),DistSensPos(:,2),'g*'); %from distance sensor
legend('Reference','Encoder','Camera','Distance Sensors');
% legend('Reference','Camera','Distance Sensors');
% legend('Encoder','Camera','Distance Sensors');
title('Position data')
set(gcf, 'Position', get(0,'Screensize'));
% saveas(gcf, 'noise_increased.png');
% Animation of the path
% figure(2)
% hold on
% for i=1:100:length(RefPos(:,1))
%     plot(EncPos(i,1),EncPos(i,2),'r*')
%     pause(.001);
% end
% the error is accumulated