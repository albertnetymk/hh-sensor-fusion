%% load data from file
clear all
% load the data
load Data

% add noise to the encoder signal and estimate position
noise = randn(size(Enc))/10;
EncPos = position_from_encoder(Enc+noise);

% % plot the data
% figure; hold on;
% plot(RefPos(:,1),RefPos(:,2),'k*'); % reference data
% plot(EncPos(:,1),EncPos(:,2),'r*'); % from encoder signal
% plot(CamPos(:,1),CamPos(:,2),'m*'); % from camera
% plot(DistSensPos(:,1),DistSensPos(:,2),'g*'); %from distance sensor
% legend('Reference','Encoder','Camera','Distance Sensors');
% title('Position data')
% 
% %%
% % simple average
% simple_average_Pos = (EncPos + CamPos + DistSensPos)/3;
% figure; hold on;
% plot(RefPos(:,1),RefPos(:,2),'k*')
% plot(simple_average_Pos(:,1),simple_average_Pos(:,2),'r*')
% title('Simple average');
% 
% %%
% % simple weighted average
% weighted_average_Pos = (0.5*EncPos + 0.4*CamPos + 0.1*DistSensPos);
% close all
% figure; hold on;
% plot(RefPos(:,1),RefPos(:,2),'k*')
% plot(weighted_average_Pos(:,1),weighted_average_Pos(:,2),'g*');
% title('Weighted average');
% 
% %% data dependent weighted averaging
% close all
% 
% % 1D membership function for camera data x-axis
% res = 0.01;
% x = 0:res:12;
% temp = zeros(size(x));
% temp = pdf('norm',x,2,0.5)+pdf('norm',x,10,0.5);
% temp = temp/max(temp);
% temp(1:200)=1;
% temp(1000:end)=1;
% X_Cam = ones(size(x))-temp;
% clear temp
% 
% % 1D membership function for camera data y-axis
% y=0:res:16;
% temp = zeros(size(y));
% temp = pdf('norm',y,2,0.5)+pdf('norm',y,13,1);
% temp=temp/max(temp);
% temp(1:200)=1;
% temp(1300:end)=1;
% Y_Cam = ones(size(y))-temp;
% clear temp
% 
% 
% % Create 2D mesh-grid for membership functions
% [xx,yy]= meshgrid(x,y);
% [Cam_pdfxx,Cam_pdfyy] = meshgrid(X_Cam,Y_Cam);
% 
% % Calculate 2D combined membership function for camera data
% % assuming the 1D membership functions are independent
% Cam_pdfxy = Cam_pdfxx.*Cam_pdfyy; 
% 
% % create 2D membership function for distance sensor
% DistSens_pdfxy = ones(length(y),length(x))-Cam_pdfxy;
% 
% % Plot the results
% figure; mesh(xx,yy,Cam_pdfxy)
% figure;
% mesh(xx,yy,DistSens_pdfxy)
% 
% % weighted sensor data
% WeightedCamPos = calculate_weighted_data(x, y, Cam_pdfxy, CamPos, CamPos);
% WeightedDistSensPos = calculate_weighted_data(x, y, DistSens_pdfxy, DistSensPos, CamPos);
% 
% %
% % data dependent weighted average
% CombinedPos = WeightedCamPos + WeightedDistSensPos;
% figure; hold on;
% plot(RefPos(:,1),RefPos(:,2),'k*')
% plot(CombinedPos(:,1),CombinedPos(:,2),'m*');

%%
% encoder data with reset based on combined camera and distance sensor
% update every 'RefPeriod' samples
RefPeriod = 100;
EncPosWithReset = position_from_encoder_with_reset(Enc, CamPos, RefPeriod);
 
close all
figure; hold on;
plot(RefPos(:,1),RefPos(:,2),'k*');
plot(EncPos(:,1),EncPos(:,2),'r*');
plot(EncPosWithReset(:,1),EncPosWithReset(:,2),'g*');
legend('reference','encoder','encoder with camera and distance sensor reset')
title('encoder with camera and distance sensor reset');

set(gcf, 'Position', get(0,'Screensize')); % Maximize figure.






