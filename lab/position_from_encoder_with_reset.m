function [EncPos,Yaw] = position_from_encoder_with_reset(Enc, RefPos, RefPeriod)
% reconstruct trajectory from wheel encoder signal
% start at (2,2), yaw =0
% EncPos(:,1) -> x axis 
% EncPos(:,2) -> y axis
EncPos = [2 2];
Yaw = 0;
D=0;
L=0.8;
rEnc=0.25;
M = length(Enc);
i=0;
for m=2:M
    i=i+1;
    for t=1:2
        if Enc(t,m) < Enc(t,m-1)-pi 
            Enc(t,m:end) = Enc(t,m:end) + 2*pi ;
        elseif Enc(t,m) > Enc(t,m-1)+pi 
            Enc(t,m:end) = Enc(t,m:end) - 2*pi;
        end
    end    
    Yaw(m) = Yaw(m-1) + ((Enc(1,m)-Enc(1,m-1))-(Enc(2,m)-Enc(2,m-1)))*rEnc/L;
    D(m) = (Enc(2,m)-Enc(2,m-1) + Enc(1,m)-Enc(1,m-1))*rEnc/2;    
    EncPos(m,1) = EncPos(m-1,1) + D(m)*cos(Yaw(m)); % x axis
    EncPos(m,2) = EncPos(m-1,2) + D(m)*sin(Yaw(m)); % y axis
    if i==RefPeriod
        i=0;
        EncPos(m,1)=RefPos(m,1);
        EncPos(m,2)=RefPos(m,2);
    end
end