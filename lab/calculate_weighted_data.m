function WeightedSensorData = calculate_weighted_data(x, y, membershipFunction2D, SensorData, RefPos)

M=length(SensorData);
WeightedSensorData=[0 0];

for m=1:M
    [dummy,indX] = min(abs(x-RefPos(m,1)));
    [dummy,indY] = min(abs(y-RefPos(m,2)));
    
    WeightedSensorData(m,2)=membershipFunction2D(indY,indX)*SensorData(m,2);    
    WeightedSensorData(m,1)=membershipFunction2D(indY,indX)*SensorData(m,1);
end