function [EncPos,Yaw] = position_from_encoder(Enc)
% reconstruct trajectory from wheel encoder signal
% start at (2,2), yaw =0
% EncPos(:,1) -> x axis 
% EncPos(:,2) -> y axis
EncPos = [2 2];
Yaw = 0;
D=0;
L=0.8; %distance between wheels
rEnc=0.25; %radius of wheel
M = length(Enc);
for m=2:M
    Yaw(m) = Yaw(m-1) + ((Enc(1,m)-Enc(1,m-1))-(Enc(2,m)-Enc(2,m-1)))*rEnc/L;
    D(m) = (Enc(2,m)-Enc(2,m-1) + Enc(1,m)-Enc(1,m-1))*rEnc/2;    
    EncPos(m,1) = EncPos(m-1,1) + D(m)*cos(Yaw(m)); % x axis
    EncPos(m,2) = EncPos(m-1,2) + D(m)*sin(Yaw(m)); % y axis
end