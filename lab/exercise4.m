
clear
close all
% load the data
load Data


% 1D membership function for camera data x-axis
res = 0.01;
x = 0:res:12;
temp = zeros(size(x));
temp = pdf('norm',x,2,0.5)+pdf('norm',x,10,0.5);

temp = temp/max(temp);
temp(1:200)=1;
temp(1000:end)=1;
X_Cam = ones(size(x))-temp;
clear temp

% 1D membership function for camera data y-axis
y=0:res:16;
temp = zeros(size(y));
temp = pdf('norm',y,2,0.5)+pdf('norm',y,13.4,0.5);

temp=temp/max(temp);
temp(1:200)=1;
temp(1300:end)=1;
Y_Cam = ones(size(y))-temp;
clear temp
% Create 2D mesh-grid for membership functions
[xx,yy]= meshgrid(x,y);
[Cam_pdfxx,Cam_pdfyy] = meshgrid(X_Cam,Y_Cam);
% Calculate 2D combined membership function for camera data
% assuming the 1D membership functions are independent
Cam_pdfxy = Cam_pdfxx.*Cam_pdfyy;
% create 2D membership function for distance sensor
DistSens_pdfxy = ones(length(y),length(x))-Cam_pdfxy;

% Plot the results
% figure(1); mesh(xx,yy,Cam_pdfxy)
% figure(2);mesh(xx,yy,DistSens_pdfxy)
% weighted sensor data

% % TODO have one look at calculate_weighted_data
WeightedCamPos = calculate_weighted_data(x, y, Cam_pdfxy, CamPos, CamPos);
WeightedDistSensPos = calculate_weighted_data(x, y, DistSens_pdfxy, DistSensPos, CamPos);
% data dependent weighted average
CombinedPos = WeightedCamPos + WeightedDistSensPos;
figure(3); hold on
plot(RefPos(:,1),RefPos(:,2),'k*')
plot(CombinedPos(:,1),CombinedPos(:,2),'m*');
% set(gcf, 'Position', get(0,'Screensize'));
% saveas(gcf, 'pdf_revised.png');
% diff_x = RefPos(:,1) - CombinedPos(:,1);
% diff_y = RefPos(:,2) - CombinedPos(:,2);
% figure(3); hold on;
% for i=1:50:length(RefPos)
%     figure(3);hold on;
%     plot(RefPos(i,1),RefPos(i,2),'k*')
%     plot(CombinedPos(i,1),CombinedPos(i,2),'m*');
%     figure(4);hold on;
%     subplot(2,1,1);plot(i, diff_x(i),'*');
%     pause(.01);
% end

% figure(4)
% subplot(2,1,1);plot(diff_x);
% subplot(2,1,2);plot(diff_y);
mse = mean((RefPos(:,1)-CombinedPos(:,1)).^2+ ...
    (RefPos(:,2)-CombinedPos(:,2)).^2)

RefPeriod = 100;

EncPosWithReset = position_from_encoder_with_reset(Enc, CombinedPos, RefPeriod);

figure(4); hold on;

plot(RefPos(:,1),RefPos(:,2),'k*');
% plot(EncPos(:,1),EncPos(:,2),'r*');
plot(EncPosWithReset(:,1),EncPosWithReset(:,2),'g*');
legend('reference','encoder', ...
    'encoder with camera and distance sensor reset')
title('encoder with camera and distance sensor reset');
set(gcf, 'Position', get(0,'Screensize'));
saveas(gcf, 'pdf_period.png');
mse = mean((RefPos(:,1)-EncPosWithReset(:,1)).^2+ ...
    (RefPos(:,2)-EncPosWithReset(:,2)).^2)