clc;
clear
close all
% load the data
load Data
% add noise to the encoder signal and estimate position
noise = randn(size(Enc))/10;
EncPos = position_from_encoder(Enc+noise);

% simple average
simple_average_Pos = (EncPos + CamPos + DistSensPos)/3;
figure(1); hold on;
plot(RefPos(:,1),RefPos(:,2),'k*')

plot(simple_average_Pos(:,1),simple_average_Pos(:,2),'r*')
error_distance = mean(sqrt((RefPos(:,1)-simple_average_Pos(:,1)).^2+ ...
    (RefPos(:,2)-simple_average_Pos(:,2)).^2))

mse_encoder = mean((RefPos(:,1)-EncPos(:,1)).^2+ ...
    (RefPos(:,2)-EncPos(:,2)).^2)

mse_camare = mean((RefPos(:,1)-CamPos(:,1)).^2+ ...
    (RefPos(:,2)-CamPos(:,2)).^2)

mse_distanceSensor = mean((RefPos(:,1)-DistSensPos(:,1)).^2+ ...
    (RefPos(:,2)-DistSensPos(:,2)).^2)

mse = mean((RefPos(:,1)-simple_average_Pos(:,1)).^2+ ...
    (RefPos(:,2)-simple_average_Pos(:,2)).^2)

title('Simple average');
set(gcf, 'Position', get(0,'Screensize'));
saveas(gcf, 'simple_average.png');
