clc
clear
close all
% load the data
load Data
% add noise to the encoder signal and estimate position
noise = randn(size(Enc))/10;
EncPos = position_from_encoder(Enc+noise);

weighted_average_Pos = (0.5*EncPos + 0.4*CamPos + 0.1*DistSensPos);
close all
figure; hold on;
plot(RefPos(:,1),RefPos(:,2),'k*')
plot(weighted_average_Pos(:,1),weighted_average_Pos(:,2),'g*');

mse_encoder = mean((RefPos(:,1)-EncPos(:,1)).^2+ ...
    (RefPos(:,2)-EncPos(:,2)).^2)

mse_camare = mean((RefPos(:,1)-CamPos(:,1)).^2+ ...
    (RefPos(:,2)-CamPos(:,2)).^2)

mse_distanceSensor = mean((RefPos(:,1)-DistSensPos(:,1)).^2+ ...
    (RefPos(:,2)-DistSensPos(:,2)).^2)

mse = mean((RefPos(:,1)-weighted_average_Pos(:,1)).^2+ ...
    (RefPos(:,2)-weighted_average_Pos(:,2)).^2)


title('Weighted average');
set(gcf, 'Position', get(0,'Screensize'));
saveas(gcf, 'weighted_average.png');