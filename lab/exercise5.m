clear
close all
% load the data
load Data

% add noise to the encoder signal and estimate position
noise = randn(size(Enc))/10;
EncPos = position_from_encoder(Enc+noise);

RefPeriod = 200;
EncPosWithReset = position_from_encoder_with_reset(Enc, CamPos, RefPeriod);

figure; hold on;

plot(RefPos(:,1),RefPos(:,2),'k*');
plot(EncPos(:,1),EncPos(:,2),'r*');
plot(EncPosWithReset(:,1),EncPosWithReset(:,2),'g*');
legend('reference','encoder', ...
    'encoder with camera and distance sensor reset')
title('encoder with camera and distance sensor reset');
set(gcf, 'Position', get(0,'Screensize')); % Maximize figure.
saveas(gcf, 'period_increased.png');
mse = mean((RefPos(:,1)-EncPosWithReset(:,1)).^2+ ...
    (RefPos(:,2)-EncPosWithReset(:,2)).^2)
% figure(2);hold on
% for i=1:10:length(RefPos)
%     plot(RefPos(i,1), RefPos(i,2), '*k')
%     plot(EncPosWithReset(i,1), EncPosWithReset(i, 2), '*g')
%     pause(.01)
% end
